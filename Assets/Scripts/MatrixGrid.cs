﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatrixGrid : MonoBehaviour {

	public static int row = 10;
	public static int column = 20;
	public static float done  = 1f;
	public static Transform[,] grid=new Transform[row,column];

	private static PlayerUi playerUi;

	public static Vector2 RoundVector(Vector2 v){
		return new Vector2 (Mathf.Round (v.x), Mathf.Round (v.y));
	}

	public static bool IsInsideBorder(Vector2 pos){
		/*if ((int)pos.y >= 16) {
			Debug.Log ("y>16");
			Time.timeScale = 0f;
			return true;
		}*/
		return ((int)pos.x >= 0 && (int)pos.x < row && (int)pos.y >= 0);
	}

	public static void DeleteRow(int y) {
		for (int x = 0; x < row; ++x) {
			GameObject.Destroy (grid [x, y].gameObject);
			grid [x, y] = null; //update grid
		}
	}

	public static void DecreaseRow(int y){
		for (int x = 0; x < row; ++x) {
			if (grid [x, y] != null) {
				grid [x, y - 1] = grid [x, y];
				grid [x, y] = null; //update grid

				grid [x, y - 1].position += new Vector3 (0,-1,0); //bajo una fila la posicion del objeto
			}
		}
	}

	public static void DecreaseRowAbove(int y){
		for (int i = y; i < column; ++i) {
			DecreaseRow (i);
		}
	}

	public static bool IsRowFull(int y){
		for (int x = 0; x < row; ++x) {
			if (grid [x, y] == null) //if not full return false
				return false;
		}
		return true;
	}

	private static Vector3 FullRowCharges(int y){
		Vector3 totalCharges = new Vector3();
		for (int x = 0; x < row; ++x) {
			if (grid [x, y] != null) { //we assume that IsRowFull was called before and gives us true
				totalCharges += grid[x,y].gameObject.GetComponent<TetrisBlockColor>().GetColors();
			}
		}
		Debug.Log (totalCharges);
		return totalCharges;
	}

	public static void DeleteWholeRow(){
		playerUi=GameObject.FindGameObjectWithTag ("GameManager").GetComponent<PlayerUi> ();
		for (int y = 0; y < column; y++) {
			if (IsRowFull (y)) {
				Vector3 charges = FullRowCharges (y);
				DeleteRow (y);
				DecreaseRowAbove (y + 1);

				done -= 0.1f;
				playerUi.SetEnemyHpSliderValue (done);

				Player.Instance().ChargeMagic ((int)charges.x, (int)charges.y, (int)charges.z);
				--y;
				if (done <= 0f) {
					SceneManager.LoadScene("end");
				}
			}
		}
	}

}
