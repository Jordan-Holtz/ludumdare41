﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class endManager : MonoBehaviour {

	public Text text;
	public Button rButton;
	public Button lButton;
	public Button cButton;

	public bool optionr;
	public bool optionl;
	public int option = 0;

	private enum States
	{
		question, trueState, falseState
	};
	private States myState;
	private string[] textos = new string[10];

	// Use this for initialization
	void Start () {
		myState = States.question;
		textos [0] = "We live in a time full of mediocrity. Office work is almost the only way to work.";
		textos [1] = "Occacionally an oportunity is opened, but who am i to be able to win that space? if i'm just another clerck who runs a computer and dream of being a hacker.";
		textos [2] = "Unknown: 2143443fdsafgau34t6a76sd5f6sdgfsdf7gr35ds6fgsdfhdsgf956fsgdf96sdg56";
		textos [3] = " It must be a joke in the Systems sector. But I have nothing better to do, let's see what they say.";
		textos [4] = "Decrypting: We need you. We are in danger. The cyber attacks are coming and we do not have a good defense. If you can read this ... we need you.";
		textos [5] = "Quite elaborate for a joke of the systems. They usually end with a black these jokes.";
		textos [6] = "I will answer the message. Surely afterwards they paste my answer in the board as the time I answered the survey of the wallet insperctor. But, what if it's true?";
		textos [7] = "[ligth go off and on] [No internet]";	
	}

	// Update is called once per frame
	void Update () {
		if (myState == States.question) {
			question ();
		} else if (myState == States.trueState) {trueState ();}
		else if (myState == States.falseState) {falseState ();}
	}

	void OnEnable(){
		rButton.onClick.AddListener (delegate {
			optionr = true;
		});
		lButton.onClick.AddListener (delegate {
			optionl = true;
		});
		cButton.onClick.AddListener (delegate {
			if (option <7){
				option += 1;
			}
			else
			{
				SceneManager.LoadScene("PlayGame");
			}
		});
	}
	void question(){

		text.text = textos [option];
		if (option < 7) {
			rButton.gameObject.SetActive (false);
			lButton.gameObject.SetActive (false);

		} else {
			SceneManager.LoadScene("PlayGame");
			cButton.gameObject.SetActive (false);
		}

		if(optionr == true){myState = States.trueState;} 
		else if (optionl == true) {	myState = States.falseState;}
	}
	void trueState(){
		text.text = "Es correcto";
	}
	void falseState(){
		text.text = "Es falso";
	}

}
