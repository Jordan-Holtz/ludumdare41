﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUi : MonoBehaviour {

	public Slider hpSlider;
	public Slider enemyHpSlider;

	public Slider fireChargeSlider;
	public Slider natureChargeSlider;
	public Slider waterChargeSlider;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetSliderValue(float percentage){
		if(percentage<=1f && percentage>=0f)
			hpSlider.value = percentage;
	}

	public void SetEnemyHpSliderValue(float percentage){
		if(percentage<=1f && percentage>=0f)
			enemyHpSlider.value = percentage;
	}

	public void SetChargeSliderValue(Vector3 charges){
		if(charges.x<=1f && charges.x>=0f)
			fireChargeSlider.value = charges.x;
		if(charges.y<=1f && charges.y>=0f)
			natureChargeSlider.value = charges.y;
		if(charges.z<=1f && charges.z>=0f)
			waterChargeSlider.value = charges.z;
	}
		
}
