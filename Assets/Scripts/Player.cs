﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[SerializeField]
	private int waterMagic;

	[SerializeField]
	private int fireMagic;

	[SerializeField]
	private int natureMagic;

	private float totalLife;
	private float life;
	private int normalDamage;

	public bool cheats;

	private static Player instance;
	public static Player Instance() {
			if (instance == null) {
				GameObject go = GameObject.FindGameObjectWithTag("GameManager");
				instance = go.GetComponentInChildren<Player> ();
			}

			return instance;
	}

	// Use this for initialization
	void Start () {
		waterMagic = 0;
		fireMagic = 0;
		natureMagic = 0;
		cheats = true;
		totalLife = 100f;
		life = 100f;
	}
	
	// Update is called once per frame
	void Update () {
		if (cheats && Input.GetKeyDown (KeyCode.Space)) {
			Vector3 suma = new Vector3 (1, 0, 0) + new Vector3 (1, 0, 0) + new Vector3 (0, 1, 0);
			Debug.Log (suma+" xyz: "+(int)suma.x+(int)suma.y+(int)suma.z);
			ChargeMagic (5,2,3);
		}
	}

	public void ChargeMagic(int fireCharge, int natureCharge, int waterElem){
		waterMagic += waterElem;
		fireMagic += fireCharge;
		natureMagic += natureCharge;

		gameObject.GetComponent<PlayerUi> ().SetChargeSliderValue (new Vector3(fireMagic/5f,natureMagic/5f,waterMagic/5f));
		Debug.Log ("charges: Fire-"+fireMagic
			+ " / Nature-"+natureMagic
			+ " / Water-"+waterMagic);
	}

	//returns the porcentage of actual life
	public float ChangeLife(int amount){
		life += amount;
		return life/totalLife;
	}


}
