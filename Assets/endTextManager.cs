﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class endTextManager : MonoBehaviour {

	public Text text;
	public Button rButton;
	public Button lButton;
	public Button cButton;

	public bool optionr;
	public bool optionl;
	public int option = 0;

	private enum States
	{
		question, trueState, falseState
	};
	private States myState;
	private string[] textos = new string[10];

	// Use this for initialization
	void Start () {
		myState = States.question;
		myState = States.question;
		textos [0] = "Finally i have internet!";
		textos [1] = "I should check the logs. What's happened?";
		textos [2] = "There are several attepmt to enter for a 200.xxx.xxx.xxx IP";
		textos [3] = "Danm Argentinian! I know there are vengeful persons!";
		textos [4] = "Not bother anymore. All 200 ip blocked!";
		textos [5] = "Hooray!";
	}

	// Update is called once per frame
	void Update () {
		if (myState == States.question) {
			question ();
		} else if (myState == States.trueState) {trueState ();}
		else if (myState == States.falseState) {falseState ();}
	}

	void OnEnable(){
		rButton.onClick.AddListener (delegate {
			optionr = true;
		});
		lButton.onClick.AddListener (delegate {
			optionl = true;
		});
		cButton.onClick.AddListener (delegate {
			if (option <5){
				option += 1;
			}
			else
			{
				SceneManager.LoadScene("menu");
			}
		});
	}
	void question(){

		text.text = textos [option];
		if (option < 5) {
			rButton.gameObject.SetActive (false);
			lButton.gameObject.SetActive (false);

		} else {
			SceneManager.LoadScene("menu");
			cButton.gameObject.SetActive (false);
		}

		if(optionr == true){myState = States.trueState;} 
		else if (optionl == true) {	myState = States.falseState;}
	}
	void trueState(){
		text.text = "Es correcto";
	}
	void falseState(){
		text.text = "Es falso";
	}

}
