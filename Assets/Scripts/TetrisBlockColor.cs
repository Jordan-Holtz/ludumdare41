﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisBlockColor : MonoBehaviour {

	private int color;

	// Use this for initialization
	void Start () {
		color = Random.Range (0, 3);
		if (color == 0)
			gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
		else if (color == 1)
			gameObject.GetComponent<SpriteRenderer> ().color = Color.green;
		else if (color == 2)
			gameObject.GetComponent<SpriteRenderer> ().color = Color.blue;
		else
			gameObject.GetComponent<SpriteRenderer> ().color = Color.black;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Vector3 GetColors(){
		if (color == 0)
			return new Vector3 (1, 0, 0);
		else if (color == 1)
			return new Vector3 (0, 1, 0);
		else if (color == 2)
			return new Vector3 (0, 0, 1);
		else
			return new Vector3 (1, 1, 1);
	}
}
