﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuController : MonoBehaviour {

	public void PlayGame(){
		SceneManager.LoadScene("intro");
	}

	public void MenuScene(){
		SceneManager.LoadScene("menu");
	}

	public void GameDescriptionScene(){
		SceneManager.LoadScene("GameDescription");
	}

	public void CreditsScene(){
		SceneManager.LoadScene("Credits");
	}

	public void ExitScene(){
		Application.Quit ();
	}

}
