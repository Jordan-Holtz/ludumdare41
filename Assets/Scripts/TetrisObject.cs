﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisObject : MonoBehaviour {

	float lastFall = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			transform.position += new Vector3 (-1, 0, 0);

			if (IsValidGridPosition ()) {
				UpdateMatrixGrid ();

			} else { 
				transform.position += new Vector3 (1, 0, 0); //reverse the movement becouse we cant go there
			}
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			transform.position += new Vector3 (1, 0, 0);

			if (IsValidGridPosition ()) {
				UpdateMatrixGrid ();

			} else { 
				transform.position += new Vector3 (-1, 0, 0); //reverse the movement becouse we cant go there
			}

		} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			transform.Rotate (new Vector3 (0, 0, -90));

			if (IsValidGridPosition ()) {
				UpdateMatrixGrid ();

			} else { 
				transform.Rotate (new Vector3 (0, 0, 90));//reverse the movement becouse we cant go there
			}

		} else if (Input.GetKeyDown (KeyCode.DownArrow) || //apretaste abajo o
			Time.time - lastFall >= 0.7f) { //la figura debe seguir cayendo
			transform.position += new Vector3 (0, -1, 0);

			if (IsValidGridPosition ()) {
				UpdateMatrixGrid ();

			} else if(IsCeilPosition()){
				Time.timeScale = 0f;
			} else { 
				transform.position += new Vector3 (0, 1, 0); //reverse the movement becouse we cant go there
				MatrixGrid.DeleteWholeRow();
				FindObjectOfType<Spawner> ().SpawnRandom ();

				enabled = false;
			}
			lastFall = Time.time;
		}
	}

	bool IsValidGridPosition(){
		foreach (Transform child in transform) {
			Vector2 v = MatrixGrid.RoundVector (child.position);

			if (!MatrixGrid.IsInsideBorder (v))
				return false; //we are outside

			if (MatrixGrid.grid [(int)v.x, (int)v.y] != null && MatrixGrid.grid [(int)v.x, (int)v.y].parent != transform)
				return false;
		}
		return true;
	}

	bool IsCeilPosition(){
		foreach (Transform child in transform) {
			Vector2 v = MatrixGrid.RoundVector (child.position);
/*
			if (!MatrixGrid.IsInsideBorder (v))
				return false; //we are outside
*/
			if (v.y>=0 && MatrixGrid.grid [(int)v.x, (int)v.y] != null && MatrixGrid.grid [(int)v.x, (int)v.y].parent != transform
				&& v.y>=13)
				return true;
		}
		return false;
	}

	void UpdateMatrixGrid(){
		for (int y = 0; y < MatrixGrid.column; ++y) {
			for (int x = 0; x < MatrixGrid.row; ++x) {
				if (MatrixGrid.grid [x, y] != null) {
					if (MatrixGrid.grid [x, y].parent == transform) {
						MatrixGrid.grid [x, y] = null; //removing element
					}
						
				}
					
			}
		}

		foreach (Transform child in transform) { //adding new children
			Vector2 v = MatrixGrid.RoundVector (child.position);
			MatrixGrid.grid [(int)v.x, (int)v.y] = child;
		}
	}
}
