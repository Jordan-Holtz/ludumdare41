﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int life;
	public int damage;
	public int element;
	public float wrath; //medidor de ira

	float lastAttack = 0f;

	// Use this for initialization
	void Start () {
		wrath = 3f;
	}
	
	// Update is called once per frame
	void Update () {

		//attack player
		if (Time.time - lastAttack >= wrath) {
			//float actualPlayerLife= gameObject.GetComponent<Player> ().Instance().ChangeLife (-damage);

			float actualPlayerLife = Player.Instance ().ChangeLife (-damage);
			gameObject.GetComponent<PlayerUi> ().SetSliderValue (actualPlayerLife); //updateUi
			if (actualPlayerLife <= 0f) {
				Time.timeScale = 0f;
				//Mostrar pantalla de fin
			}
			lastAttack = Time.time;
		}
	}
}
