﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameplayController : MonoBehaviour {

	[SerializeField]
	private GameObject pausePanel;

	[SerializeField]
	private GameObject dialoguePanel;

	[SerializeField]
	private GameObject battlePanel;

	// Use this for initialization
	void Awake () {
		pausePanel.SetActive (false);
		dialoguePanel.SetActive (false);
		battlePanel.SetActive (true);
	}
	
	// Update is called once per frame
	public void Pausegame () {
		pausePanel.SetActive (true);
		Time.timeScale = 0f;
	}

	public void ResumeGame(){
		Time.timeScale = 1f;
		pausePanel.SetActive (false);
	}

	public void QuitGame(){
		Time.timeScale = 1f;
		Application.Quit ();
	}
}
